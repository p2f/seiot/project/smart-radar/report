\newpage

\section{Design}

In fase di analisi, per mantenere un livello di astrazione più lontano possibile dalle meccaniche implementative, i diagrammi delle macchine a stati finiti non sono stati concepiti per essere integrati direttamente con la decomposizione per \textit{task} del paradigma a \textit{loop}.

\subsection{Architettura}

Nel diagramma delle classi in figura \ref{fig:class-tasks} è possibile osservare la decomposizione per task gestiti dal componente \textit{Scheduler}.

\begin{figure}[!ht]
\begin{subfigure}[b]{1.1\textwidth}
\begin{plantuml}
	@startuml
	scale 2
skinparam groupInheritance 3
left to right direction

interface Task {
    void init()
    void tick()
}

interface Scheduler {
    Task *tasks
    void init()
    void schedule()
    bool addTask()
}

Scheduler "1" -- "*" Task

class MovementTask
class MotionSensorTask
class ScanTask
class SerialIOTask
class SleepTask

Task <|-- SleepTask
Task <|-- SerialIOTask
Task <|-- SwitchModeTask
Task <|-- ScanTask
Task <|-- MovementTask
Task <|-- MotionSensorTask 
	@enduml
\end{plantuml}
\end{subfigure}
	\caption{Decomposizione a task del sistema Smart Radar.}
	\label{fig:class-tasks}
\end{figure}

\newpage

\subsection{Design dettagliato}

Nei diagrammi che seguono vengono illustrati tutti i task del sistema e i componenti hardware/software, opportunamente modellati ad oggetti, con cui ognuno di essi interagisce.

\subsubsection{Movimento}

In figura \ref{fig:class-movement} viene mostrato il task relativo al movimento del Radar, che si occupa della gestione del motore e del potenziometro legato alla lettura del tempo di una scansione.

\begin{figure}[!ht]
\centering
 \begin{plantuml}
	@startuml

enum Modes {
    SINGLE
    MANUAL
    AUTO
}

class MovementTask
interface Servo {
    void moveLeft()
    void moveRight()
}

interface Potentiometer {
    uint read()
}

class PotentiometerImpl {
    uint pin
    uint min
    uint max
    uint PotentiometerImpl(uint pin, uint min, uint max)
    uint read()
}

class ServoImpl {
    uint pin
    uint ServoImpl(uint pin)
    void moveLeft()
    void moveRight()
}

MovementTask -- Modes
MovementTask "1" o-- "1" Potentiometer
MovementTask "1" o-- "1" Servo
Potentiometer <|-- PotentiometerImpl
Servo <|-- ServoImpl
	@enduml
\end{plantuml}
	\caption{Architettura del task legato al movimento del sistema Smart Radar.}
	\label{fig:class-movement}
\end{figure}

\newpage

\subsubsection{Rilevazione movimento}

In figura \ref{fig:class-motion} viene mostrato il task relativo alla rilevazione del movimento attuata in modalità ``Single'' utilizzando il componente PIR.

\begin{figure}[!ht]
\centering
 \begin{plantuml}
	@startuml
	scale 0.7
enum Modes {
    SINGLE
    MANUAL
    AUTO
}

class MotionSensorTask

interface MotionSensor {
    bool read()
}

class MotionSensorImpl {
    - uint pin
    - bool isCalibrated
    - uint calibrationDuration
    + MotionSensorImpl(uint pin)
    + bool read()
}

MotionSensorTask -- Modes
MotionSensorTask "1" o-- "1" MotionSensor
MotionSensor <|-- MotionSensorImpl
	@enduml
\end{plantuml}
	\caption{Architettura del task legato alla rilevazione del movimento del sistema Smart Radar.}
	\label{fig:class-motion}
\end{figure}

\newpage

\subsubsection{Scansione}

In figura \ref{fig:class-scan} viene mostrato il task relativo alle scansioni attuate dal sistema utilizzando il lettore ad ultrasuoni.
È possibile notare che tale task gestisce anche i LED LA e LD.

\begin{figure}[!ht]
\centering
 \begin{plantuml}
	@startuml
	scale 0.7
	enum Modes {
    SINGLE
    MANUAL
    AUTO
}

class ScanTask

interface UltrasonicSensor {
    + double read()
}

interface Led {
    + void switchOn()
    + void switchOff()
    + bool isTurnedOn()
}

class UltrasonicSensorImpl {
    + UltrasonicSensorImpl(uint triggerPin, uint echoPin)
    + double read()
}

class LedImpl {
    - uint pin
    - bool isOn
    + LedImpl(uint pin)
    + void switchOn()
    + void switchOff()
    + bool isTurnedOn()
}

ScanTask -- Modes
ScanTask "1" o-- "1" UltrasonicSensor
ScanTask "1" o-- "2" Led
UltrasonicSensor <|-- UltrasonicSensorImpl
Led <|-- LedImpl
	@enduml
\end{plantuml}
	\caption{Architettura del task legato alla scansione operata dal sistema Smart Radar.}
	\label{fig:class-scan}
\end{figure}

\newpage

\subsubsection{Cambio modalità}

In figura \ref{fig:class-mode} viene mostrato il task relativo al cambio di modalità del sistema utilizzando unicamente gli interruttori tattili.
Si consulti il paragrafo \ref{sec:serialiotask} per il cambio di modalità operato dalla Console.

\begin{figure}[!ht]
\centering
 \begin{plantuml}
	@startuml
	scale 0.7
	enum Modes {
    SINGLE
    MANUAL
    AUTO
}

class SwitchModeTask

interface Button {
    + bool isPressed()
}

class ButtonImpl {
    - uint pin
    + ButtonImpl(uint pin)
    + bool isPressed()
}

SwitchModeTask -- Modes
SwitchModeTask "1" o-- "3" Button
Button <|-- ButtonImpl
	@enduml
\end{plantuml}
	\caption{Architettura del task legato al cambio di modalità del sistema Smart Radar.}
	\label{fig:class-mode}
\end{figure}

\newpage

\subsubsection{Sleep}

In figura \ref{fig:class-sleep} viene mostrato il task relativo allo stato di ``Sleep'' in cui può trovarsi il sistema nella modalità ``Single''.

\begin{figure}[!ht]
\centering
 \begin{plantuml}
	@startuml
	scale 0.7
enum Modes {
    SINGLE
    MANUAL
    AUTO
}

class SleepTask {
    - uint pin
}

SleepTask -- Modes
	@enduml
\end{plantuml}
	\caption{Architettura del task legato allo stato di ``Sleep'' del sistema Smart Radar.}
	\label{fig:class-sleep}
\end{figure}

\subsubsection{Serial}
\label{sec:serialiotask}

In figura \ref{fig:class-serial} viene mostrato il task relativo alla lettura e scrittura di messaggi nella Seriale del controllore.

\begin{figure}[!ht]
\centering
 \begin{plantuml}
	@startuml
	scale 0.5
enum Modes {
    SINGLE
    MANUAL
    AUTO
}

class Serial <<external>> {
    + {static} void begin(long baud)
    + {static} size_t println(String s)
    + {static} int available()
    + {static} int read()
}

class SerialIOTask {
    - MsgService msgService
}

class MsgService {
    + Msg currentMsg
    + bool msgAvailable
    + void init()
    + bool isMsgAvailable()
    + Msg receiveMsg()
    + void sendMsg(String msg)
}

class Msg {
    - String content
    + Msg(String content)
    + String getContent()
}

SerialIOTask -- Modes
SerialIOTask "1" o-- "1" MsgService
MsgService "1" o-- "1" Msg
MsgService --> Serial : <<use>>
	@enduml
\end{plantuml}
	\caption{Architettura del task legato alla lettura e scrittura di messaggi del sistema Smart Radar.}
	\label{fig:class-serial}
\end{figure}

\newpage

\subsection{Disegno Fritzing}

La fase di progettazione del sistema comprende anche la pianificazione della disposizione e del collegamento dei componenti fisici prima dell'effettiva realizzazione su breadboard.

\begin{figure}[!ht]
	\includegraphics[scale=0.4]{./02-smartradar_bb.png}
	\caption{Disegno Fritzing del circuito.}\label{fig:fritzing}
\end{figure}
