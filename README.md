# Smart Radar report

## LaTeX instructions

**NOTE**: you can also use `kile` to write LaTeX, I think it's better (mostly
for Vi mode)

- install `texlive-core`, `texlive-latexextra`, `minted`, `plantuml`, `texstudio`
- download italian dictionary from libreoffice and put it in the right folder
- import the texstudio profile ([`texstudio.txsprofile`](./texstudio.txsprofile)) in TeXstudio
- run with F5

## State diagram

## Analysis Diagram

```plantuml
hide empty description

[*] --> Manual
Manual --> Single : Tm1
Manual --> Auto : Tm3
Single --> Manual : Tm2
Single --> Auto : Tm3
Auto --> Single : Tm1
Auto --> Manual : Tm2
```

### Project diagram

#### Manual

```plantuml
hide empty description

[*] --> Fermo
Fermo --> InMovimento : left || right
InMovimento --> Fermo
Fermo --> Scansione
Scansione --> Fermo
```

#### Single

```plantuml
hide empty description

[*] --> Sleep
Sleep --> Scan : PIR
Scan --> Move
Move --> Wait : /N++
Wait --> Scan : [T == Tpot / Nspicchi]
Wait --> Sleep : [UltimoSpicchio]

state Move
```

#### Auto

```plantuml
hide empty description

state Auto {
    state AllarmeChoice <<choice>>
    state LetturaChoice <<choice>>

    Allarme: exit/inAllarme = true

    [*] --> Lettura
    Lettura --> AllarmeChoice
    AllarmeChoice --> Allarme: [D between\nDnear and Dfar]
    AllarmeChoice --> Movimento : [else]
    Lettura --> Tracking: [D < Dnear]
    Tracking --> Lettura
    Allarme --> Movimento
    Movimento --> LetturaChoice 
    LetturaChoice --> Lettura : [N == Nspicchi]/\ninAllarme=false
    LetturaChoice --> Lettura : [else]
    --

    [*] -> NotBlinking
    NotBlinking -> Blinking : [inAllarme]
    Blinking -> NotBlinking : [!inAllarme]
}
```

## Class Diagram

### Analysis diagram

```plantuml
skinparam groupInheritance 3
left to right direction

interface Task {
    void init()
    void tick()
}

interface Scheduler {
    Task *tasks
    void init()
    void schedule()
    bool addTask()
}

Scheduler "1" -- "*" Task

class MovementTask
class MotionSensorTask
class ScanTask
class SerialIOTask
class SleepTask

Task <|--- SleepTask
Task <|--- SerialIOTask
Task <|--- SwitchModeTask
Task <|--- ScanTask
Task <|--- MovementTask
Task <|--- MotionSensorTask
```

### Project diagram

#### Movement

```plantuml
enum Modes {
    SINGLE
    MANUAL
    AUTO
}

class MovementTask

interface ServoMotor {
    + uint getPosition()
    + void setPosition(uint angle)
    + void moveLeft()
    + void moveRight()
}

class ServoMotorImpl {
    - uint pin
    - ServoTimer2 motor
    + ServoImpl(uint pin)
    + uint getPosition()
    + void setPosition(uint angle)
    + void moveLeft()
    + void moveRight()
}

interface Potentiometer {
    + uint read()
}

class PotentiometerImpl {
    - uint pin
    - uint min
    - uint max
    + uint PotentiometerImpl(uint pin, uint min, uint max)
    + uint read()
}

class ServoTimer2 <<external>> {
    + attach();
    + detach();
    + write(int);
}

MovementTask -- Modes
MovementTask "1" o-- "1" Potentiometer
MovementTask "1" o-- "1" ServoMotor
Potentiometer <|-- PotentiometerImpl
ServoMotor <|-- ServoMotorImpl
ServoMotorImpl "1" o-left- "1" ServoTimer2 : <<external>>
```

### Motion detection

```plantuml
enum Modes {
    SINGLE
    MANUAL
    AUTO
}

class MotionSensorTask

interface MotionSensor {
    bool read()
}

class MotionSensorImpl {
    - uint pin
    - bool isCalibrated
    - uint calibrationDuration
    + MotionSensorImpl(uint pin)
    + bool read()
}

MotionSensorTask -- Modes
MotionSensorTask "1" o-- "1" MotionSensor
MotionSensor <|-- MotionSensorImpl
```

### Scan

```plantuml
enum Modes {
    SINGLE
    MANUAL
    AUTO
}

class ScanTask

interface UltrasonicSensor {
    + double read()
}

interface Led {
    + void switchOn()
    + void switchOff()
    + bool isTurnedOn()
}

class UltrasonicSensorImpl {
    + UltrasonicSensorImpl(uint triggerPin, uint echoPin)
    + double read()
}

class LedImpl {
    - uint pin
    - bool isOn
    + LedImpl(uint pin)
    + void switchOn()
    + void switchOff()
    + bool isTurnedOn()
}

ScanTask -- Modes
ScanTask "1" o-- "1" UltrasonicSensor
ScanTask "1" o-- "2" Led
UltrasonicSensor <|-- UltrasonicSensorImpl
Led <|-- LedImpl
```

### Switch Mode

```plantuml
enum Modes {
    SINGLE
    MANUAL
    AUTO
}

class SwitchModeTask

interface Button {
    + bool isPressed()
}

class ButtonImpl {
    - uint pin
    + ButtonImpl(uint pin)
    + bool isPressed()
}

SwitchModeTask -- Modes
SwitchModeTask "1" o-- "3" Button
Button <|-- ButtonImpl
```

### Sleep

```plantuml
enum Modes {
    SINGLE
    MANUAL
    AUTO
}

class SleepTask {
    - uint pin
}

SleepTask -- Modes
```

### Serial

```plantuml
enum Modes {
    SINGLE
    MANUAL
    AUTO
}

class Serial <<external>> {
    + {static} void begin(long baud)
    + {static} size_t println(String s)
    + {static} int available()
    + {static} int read()
}

class SerialIOTask {
    - MsgService msgService
}

class MsgService {
    + Msg currentMsg
    + bool msgAvailable
    + void init()
    + bool isMsgAvailable()
    + Msg receiveMsg()
    + void sendMsg(String msg)
}

class Msg {
    - String content
    + Msg(String content)
    + String getContent()
}

SerialIOTask -- Modes
SerialIOTask "1" o-- "1" MsgService
MsgService "1" o-- "1" Msg
MsgService --> Serial : <<use>>
```

## Fritzing schema

[Sources](https://sites.google.com/site/marcobotprojects/fritzing/pirsensor) for
the Fritzing version of the PIR sensor.

![](./img/02-smartradar_bb.png)
